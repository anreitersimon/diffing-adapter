package com.example.anreitersimon.myapplication;

import android.support.annotation.CallSuper;
import android.view.View;

/**
 * Created by anreitersimon on 24/02/2017.
 */

public abstract class ListItemHolder {
  private View mItemView = null;
  public View getItemView() {
    return mItemView;
  }

  @CallSuper
  protected void bindView(View itemView) {
    mItemView = itemView;
  }
}
