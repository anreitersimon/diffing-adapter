package com.example.anreitersimon.myapplication;

import android.view.View;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by anreitersimon on 25/02/2017.
 */

public class BaseListItemHolder extends ListItemHolder {

    @Override
    protected void bindView(View itemView) {
        super.bindView(itemView);
        ButterKnife.bind(this, itemView);
    }
}
