package com.example.anreitersimon.myapplication;

/**
 * Created by anreitersimon on 24/02/2017.
 */

interface Diffable {
  Object getIdentity();

  public boolean contentsSame(Diffable other);
}