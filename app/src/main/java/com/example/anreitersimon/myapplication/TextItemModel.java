package com.example.anreitersimon.myapplication;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import butterknife.Bind;

/**
 * Created by anreitersimon on 25/02/2017.
 */

public class TextItemModel extends ListItemModel<TextItemModel.Holder> implements View.OnClickListener {

    interface CallBack {
        public void onClick(TextItemModel model);
    }

    public class Holder extends BaseListItemHolder{
        @Bind(R.id.item_title)
        TextView title;

        @Bind(R.id.item_subtitle)
        TextView subtitle;
    }
    @Nullable Object mIdentity;

    @Nullable
    private String mTitle = null;

    @Nullable
    private String mSubTitle = null;

    @Nullable
    private CallBack mCallback = null;

    public TextItemModel callBack(@Nullable CallBack callback) {
        mCallback = callback;

        return this;
    }

    public TextItemModel identity(@Nullable Object identity) {
        mIdentity = identity;

        return this;
    }

    public TextItemModel title(String title) {
        mTitle = title;

        return this;
    }

    public TextItemModel subtitle(String subtitle) {
        mSubTitle = subtitle;

        return this;
    }

    @Override
    public Object getIdentity() {
        return mIdentity != null ? mIdentity : this;
    }

    @Override
    public boolean contentsSame(Diffable other) {
        if (!(other instanceof TextItemModel)) {
            return false;
        }
        TextItemModel o = (TextItemModel) other;

        if ( !((mTitle == null && o.mTitle == null) || mTitle.equals(o.mTitle) )) { return false; }
        if ( !((mSubTitle == null && o.mSubTitle == null) || mSubTitle.equals(o.mSubTitle) )) { return false; }

        return true;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TextItemModel)) return false;

        TextItemModel that = (TextItemModel) o;

        if (mTitle != null ? !mTitle.equals(that.mTitle) : that.mTitle != null) return false;
        return mSubTitle != null ? mSubTitle.equals(that.mSubTitle) : that.mSubTitle == null;

    }

    @Override
    public int hashCode() {
        int result = mTitle != null ? mTitle.hashCode() : 0;
        result = 31 * result + (mSubTitle != null ? mSubTitle.hashCode() : 0);
        return result;
    }

    @Override
    public int getLayout() {
        return R.layout.text_item_row;
    }

    @Override
    public Holder createViewHolder() {
        return new Holder();
    }

    @Override
    public void bind(Holder holder) {

        boolean showTitle = mTitle != null && (mTitle.length() > 0);
        boolean showSubtitle = mSubTitle != null && (mSubTitle.length() > 0);

        holder.title.setText(mTitle);
        holder.title.setVisibility(showTitle ? View.VISIBLE : View.GONE);


        holder.subtitle.setText(mSubTitle);
        holder.subtitle.setVisibility(showSubtitle ? View.VISIBLE : View.GONE);

        holder.getItemView().setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (mCallback != null) {
            mCallback.onClick(this);
        }
    }

    @Override
    public void unbind(Holder holder) {
        holder.title.setText("--not-bound--");
        holder.subtitle.setText("--not-bound--");
        holder.getItemView().setOnClickListener(null);
    }

}
