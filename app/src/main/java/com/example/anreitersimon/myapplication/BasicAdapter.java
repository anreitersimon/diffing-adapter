package com.example.anreitersimon.myapplication;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anreitersimon on 25/02/2017.
 */

public class BasicAdapter extends BaseRecyclerViewAdapter {

    public void set(List<ListItemModel<?>> items) {
        models.clear();
        models.addAll(items);

        if (!isInTransaction()) {
            notifyDataSetChanged();
        }
    }

    public void setAnimated(List<ListItemModel<?>> items) {
        beginTransaction();
        models.clear();
        models.addAll(items);
        commitTransaction();
    }

    public List<ListItemModel<?>> getModels() {
        return new ArrayList<>(models);
    }
}
