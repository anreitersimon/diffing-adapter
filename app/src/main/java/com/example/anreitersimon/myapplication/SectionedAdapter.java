package com.example.anreitersimon.myapplication;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anreitersimon on 25/02/2017.
 */

public class SectionedAdapter extends BaseRecyclerViewAdapter {

    List<Section> mSections = new ArrayList<>();

    public List<Section> getSections() {
        return new ArrayList<>(mSections);
    }

    public void setSections(List<Section> sections) {
        models.clear();
        mSections = sections;

        for (Section m: sections) {
            models.addAll(m.getFlattenedModels());
        }
        if (!isInTransaction()) {
            notifyDataSetChanged();
        }
    }

    public void setSectionsAnimated(List<Section> sections) {
        beginTransaction();
        models.clear();
        mSections = sections;

        for (Section m: sections) {
            models.addAll(m.getFlattenedModels());
        }
        commitTransaction();
    }
}
