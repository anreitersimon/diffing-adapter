package com.example.anreitersimon.myapplication;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anreitersimon on 25/02/2017.
 */

public class Section {


    ListItemModel mHeader = null;
    List<ListItemModel<?>> mItems = null;
    ListItemModel mFooter = null;

    public List<ListItemModel<?>> getFlattenedModels() {
        List<ListItemModel<?>> result = new ArrayList<>();

        if (mHeader != null) {
            result.add(mHeader);
        }
        if (mItems != null) {
            result.addAll(mItems);
        }
        if (mFooter != null) {
            result.add(mFooter);
        }
        return result;
    }

    public Section header(ListItemModel model) {
        mHeader = model;
        return this;
    }

    public Section footer(ListItemModel model) {
        mFooter = model;
        return this;
    }

    public Section items(List<ListItemModel<?>> models) {
        mItems = models;
        return this;
    }

    public <T extends ListItemModel> Section add(T model) {
        if (mItems == null) {
            mItems = new ArrayList<>();
        }
        mItems.add(model);
        return this;
    }
}