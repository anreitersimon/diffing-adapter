package com.example.anreitersimon.myapplication;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.recyclerview)
    RecyclerView mRecyclerView;


    SectionedAdapter mAdapter = new SectionedAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                mRecyclerView.getContext(),
                layoutManager.getOrientation());

        mRecyclerView.addItemDecoration(dividerItemDecoration);


        TextItemModel.CallBack c = new TextItemModel.CallBack() {
            @Override
            public void onClick(TextItemModel model) {
                Log.d("CLICK", model.toString());
            }
        };

        final List<Section> sections = new ArrayList<>();

        sections.add(
                new Section()
                        .header(new TextItemModel()
                                .title("Header-01")
                                .subtitle("SubTitle")
                                .callBack(c))
                        .add(new TextItemModel()
                                .identity(0)
                                .title("item-01")
                                .callBack(c))
                        .add(new TextItemModel()
                                .identity(1)
                                .title("item-02")
                                .callBack(c))
                        .add(new TextItemModel()
                                .identity(2)
                                .title("item-03")
                                .callBack(c))
                        .add(new TextItemModel()
                                .identity(3)
                                .title("item-04")
                                .callBack(c))
                        .footer(new TextItemModel()
                                .identity(4)
                                .title("Footer-01")
                                .subtitle("SubTitle")
                                .callBack(c)));

        sections.add(
                new Section()
                        .header(new TextItemModel()
                                .title("Header-02")
                                .subtitle("SubTitle")
                                .callBack(c))
                        .add(new TextItemModel()
                                .identity(5)
                                .title("item-01")
                                .callBack(c))
                        .add(new TextItemModel()
                                .identity(6)
                                .title("item-02")
                                .callBack(c))
                        .add(new TextItemModel()
                                .identity(7)
                                .title("item-03")
                                .callBack(c))
                        .add(new TextItemModel()
                                .identity(8)
                                .title("item-04")
                                .callBack(c))
                        .footer(new TextItemModel()
                                .title("Footer-02")
                                .subtitle("SubTitle")
                                .callBack(c)));

        mAdapter.setSections(sections);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Section> m = mAdapter.getSections();
                //Collections.reverse(m);

                for (Section s:sections) {
                    if (s.mItems != null) {
                        Collections.shuffle(s.mItems);
                    }
                }
                mAdapter.setSectionsAnimated(m);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
