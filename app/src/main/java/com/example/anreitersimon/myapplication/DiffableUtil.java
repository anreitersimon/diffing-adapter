package com.example.anreitersimon.myapplication;

import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;

import java.util.List;

/**
 * Created by anreitersimon on 24/02/2017.
 */

public class DiffableUtil {

  static <T extends Diffable> DiffUtil.DiffResult diffs( final List<T> oldItems, final List<T> newItems) {
    return diffs( oldItems, newItems,  true);
  }

  @Nullable
  static <T extends Diffable> DiffUtil.DiffResult diffs( final List<T> oldItems, final List<T> newItems, boolean detectMoves) {

    DiffUtil.Callback callback = new DiffUtil.Callback() {
      @Override
      public int getOldListSize() {
        return oldItems.size();
      }

      @Override
      public int getNewListSize() {
        return newItems.size();
      }

      @Override
      public boolean areItemsTheSame( int oldItemPosition, int newItemPosition ) {
        T oldItem = oldItems.get( oldItemPosition );
        T newItem = newItems.get( newItemPosition );

        return  oldItem.getIdentity().equals( newItem.getIdentity() );
      }

      @Override
      public boolean areContentsTheSame( int oldItemPosition, int newItemPosition ) {
        T oldItem = oldItems.get( oldItemPosition );
        T newItem = newItems.get( newItemPosition );

        return  oldItem.contentsSame( newItem );
      }
    };

    return DiffUtil.calculateDiff( callback, detectMoves);
  }
}
