package com.example.anreitersimon.myapplication;

import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

/**
 * Created by anreitersimon on 24/02/2017.
 */

public class ListItemViewHolder extends RecyclerView.ViewHolder {
  private ListItemModel mListItemModel;
  private ListItemHolder holder;

  public ListItemViewHolder( ViewGroup parent, @LayoutRes int layoutId) {
    super( LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false));
  }

  public ListItemHolder getHolder() {
    return holder;
  }

  public ListItemModel getModel() {
    return mListItemModel;
  }

  public void bind( ListItemModel listItemModel) {
    if (holder == null) {
      holder = listItemModel.createViewHolder();
      holder.bindView( itemView );
    }

    mListItemModel = listItemModel;

    mListItemModel.bind( holder );
  }

  public void unbind() {
    if (mListItemModel != null && holder != null) {
      mListItemModel.unbind( holder );
    }
    mListItemModel = null;
    holder = null;
  }
}
