package com.example.anreitersimon.myapplication;

import android.support.annotation.LayoutRes;

/**
 * Created by anreitersimon on 24/02/2017.
 */

public abstract class ListItemModel<T extends ListItemHolder> implements Diffable {

  @LayoutRes
  public abstract int getLayout();

  public abstract Object getIdentity();

  public abstract T createViewHolder();

  public abstract void bind( T holder );
  public abstract void unbind( T holder );

}
