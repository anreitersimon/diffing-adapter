package com.example.anreitersimon.myapplication;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anreitersimon on 24/02/2017.
 */

public class BaseRecyclerViewAdapter extends RecyclerView.Adapter<ListItemViewHolder> {

    abstract class Transaction {
        public abstract void execute();
    }

    protected final List<ListItemModel<?>> models = new ArrayList<>();

    private List<ListItemModel<?>> committedListItemModels = null;
    private int transactionCount = 0;

    protected final boolean isInTransaction() {
        return transactionCount > 0;
    }

    // transactions can be nested
    protected final void beginTransaction() {
        if (transactionCount == 0) {
            // save currently displayed listItemModels
            committedListItemModels = new ArrayList<>(models);
        }
        transactionCount++;
    }

    // transactions can be nested
    protected final void commitTransaction() {
        transactionCount--;

        if (transactionCount < 0) {
            throw new IllegalStateException("calls to beginTransaction() and commitTransaction() have to be symmetric");
        } else if (transactionCount == 0 && committedListItemModels != null) {

            DiffUtil.DiffResult diffs = DiffableUtil.diffs(committedListItemModels, models, true);

            if (diffs != null) {
                diffs.dispatchUpdatesTo(this);
            } else {
                notifyDataSetChanged();
            }
        }
    }

    public final void perform(Transaction transaction) {
        beginTransaction();
        transaction.execute();
        commitTransaction();
    }


    @Override
    public final int getItemViewType(int position) {
        return models.get(position).getLayout();
    }

    @Override
    public final ListItemViewHolder onCreateViewHolder(ViewGroup parent, int layoutRes) {
        return new ListItemViewHolder(parent, layoutRes);
    }

    @Override
    public final void onBindViewHolder(ListItemViewHolder holder, int position) {
        ListItemModel m = models.get(position);
        holder.bind(m);
    }

    @Override
    public final int getItemCount() {
        return models.size();
    }

    @Override
    public final void onViewRecycled(ListItemViewHolder holder) {
        holder.unbind();
    }
}
